# README #

You can use this build with e.g. docker-compose and have a build.yml in your project root. Content could look like:

```Docker-compose
sample-build:
  image: martincallesen/maven-build:3.2-jdk-8
  command: clean install -Punit-test
  volumes:
    - .:/project
  volumes_from:
    - maven-data

maven-data:
  image: centos
  command: echo 'data for maven'
  volumes:
    - ~/.m2:/root/.m2
```

### What is this repository for? ###

Build maven projects in docker

### How do I get set up? ###

Install docker and docker-compose from https://www.docker.com